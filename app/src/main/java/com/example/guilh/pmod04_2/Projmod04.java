package com.example.guilh.pmod04_2;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;
import android.widget.EditText;

public class Projmod04 extends AppCompatActivity implements View.OnClickListener {

    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_projmod04);

        Button botao = (Button)findViewById(R.id.btnAcionaButton);
        botao.setOnClickListener(this);
    }

    public void onClick(View v) {
        EditText qcoisa = (EditText)findViewById(R.id.etNomeEditText);
        TextView status = (TextView)findViewById(R.id.tvStatusTextView);
        String mensagem = "Digitou qualquer coisa " + qcoisa.getText().toString();
        status.setText(mensagem);
    }
}
